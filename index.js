const util = require("./utils");

const args = process.argv.slice(2);

const options = args.filter((arg) => arg.startsWith("-"));
const flags = args.filter((arg) => !arg.startsWith("-"));

const folder = options.includes("--dir");
const repo = options.includes("--repo");
const domain = options.includes("--domain");
const port = options.includes("--port");

// get the value attached to each flag
const folderName = folder ? flags[options.indexOf("--dir")] : "dev";
const repoName = repo ? flags[options.indexOf("--repo")] : undefined;
const domainName = domain ? flags[options.indexOf("--domain")] : undefined;
const portNumber = port ? flags[options.indexOf("--port")] : undefined;

const username = repoName.split("/")[0];
const repositoryName = repoName.split("/")[1];

util(username, repositoryName, folderName, domainName, portNumber);
